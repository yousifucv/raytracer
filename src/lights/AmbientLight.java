package lights;

import geometry.Geometry;
import geometry.Intersection;
import raytracer.Vector3;

public class AmbientLight extends Light {
	
	public final Vector3 flux;

	public AmbientLight(Vector3 flux) {
		super();
		this.flux = flux;
	}

	@Override
	public Vector3 getIlluminance(Intersection intersection,
			Iterable<Geometry> geom) {
		return flux;
	}
	
	

}
