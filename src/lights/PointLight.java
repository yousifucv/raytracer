package lights;

import geometry.Geometry;
import geometry.Intersection;
import raytracer.Ray;
import raytracer.Vector3;

public class PointLight extends Light {

	public final Vector3 position;
	public final Vector3 flux;
	
	public PointLight(Vector3 position, Vector3 flux) {
		super();
		this.position = position;
		this.flux = flux;
	}

	@Override
	public Vector3 getIlluminance(Intersection intersection, Iterable<Geometry> geom) {

		float EPSILON = 0.001f;
		
		Vector3 lightVector = intersection.location().sub(position);
		Vector3 lightDirection = lightVector.norm();
		
		float cosTheta = lightDirection.dot(intersection.normal());
		if(cosTheta < 0) {
			
			float distSq = lightVector.magSq();
			float lightDistance = (float) Math.sqrt(distSq);
			
			Ray ray = new Ray(position, lightDirection);
			boolean occluded = false;
			
			//Checking if there is anything between the light and a intersection
			for(Geometry g : geom) {
				Intersection i = g.intersectedBy(ray);
				if(i != null && (lightDistance - i.distance) > EPSILON) {
					occluded = true;
					break;
				}
			}
			
			if(!occluded) return flux.multi(-cosTheta / distSq);
		}
		
		return Vector3.zero;
	}

}
