package lights;

import geometry.Geometry;
import geometry.Intersection;
import raytracer.Ray;
import raytracer.Vector3;

abstract class DirectionalLight extends Light {

	public final Vector3 direction;
	public final Vector3 flux;

	public DirectionalLight(Vector3 sourceDiretion, final Vector3 flux) {
		super();
		this.direction = sourceDiretion.norm();
		this.flux = flux;
	}

	@Override
	public Vector3 getIlluminance(Intersection intersection,
			Iterable<Geometry> geom) {

		final float cosTheta = direction.dot(intersection.normal());

		if(cosTheta > 0) {

			final Ray ray = new Ray(intersection.location(), direction);
			boolean occluded = false;
			
			for(Geometry g : geom) {
				if(g.intersectedBy(ray) != null) {
					occluded = true;
					break;
				}
			}
			
			if(occluded) return flux.multi(cosTheta);
			
		}
		return null;
	}

}
