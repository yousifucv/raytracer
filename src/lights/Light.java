package lights;

import geometry.Geometry;
import geometry.Intersection;
import raytracer.Vector3;

public abstract class Light {

	public abstract Vector3 getIlluminance(Intersection intersection, Iterable<Geometry> geom);
	
}
