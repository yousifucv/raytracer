package scene;

import static raytracer.Utils.TAU;
import geometry.Geometry;
import geometry.Plane;
import geometry.Sphere;

import java.util.ArrayList;

import lights.AmbientLight;
import lights.Light;
import lights.PointLight;
import raytracer.Camera;
import raytracer.Vector3;

public class Scene {
	

	public ArrayList<Geometry> geometry;
	public ArrayList<Light> lights;
	public ArrayList<Camera> cameras;
	
	public Scene(ArrayList<Geometry> geometry, ArrayList<Light> lights,
			ArrayList<Camera> cameras) {
		super();
		this.geometry = geometry;
		this.lights = lights;
		this.cameras = cameras;
	}
	
	public static Scene spiralScene() {
		
		ArrayList<Geometry> geom = new ArrayList<Geometry>();
		geom.add(new Sphere(new Vector3(0,-100,0), 100));
		geom.add(new Sphere(new Vector3(0,0,-110), 100));
		
		float angle = 0f;
		while (angle < 2 * TAU) {
			float x = (float)(Math.cos(angle) * (2 * TAU - angle) * 0.1);
			float y = angle * 0.2f;
			float z = (float)(-(Math.sin(angle) * (2 * TAU - angle) * 0.1));
			
			geom.add(new Sphere(new Vector3(x, y, z), 0.02f + (2 * TAU - angle) * 0.02f));
			
			angle += TAU / 21;
		}
		
		ArrayList<Light> lights = new ArrayList<Light>();
		lights.add(new PointLight(new Vector3(-2,2,1), new Vector3(1, 0.6f, 0.2f).multi(30)));
		lights.add(new PointLight(new Vector3(2,1,2), new Vector3(1, 0.6f, 0.2f).multi(20)));
		lights.add(new PointLight(new Vector3(0,10,0), new Vector3(300)));
		lights.add(new AmbientLight(new Vector3(0.5f)));
		
		ArrayList<Camera> cameras = new ArrayList<Camera>();
		cameras.add(new Camera(new Vector3(0,3,9), new Vector3(0, 1.1f, 0), 30));
		
		return new Scene(geom, lights, cameras);
	}
	
public static Scene cornellBoxScene() {
		
		ArrayList<Geometry> geom = new ArrayList<Geometry>();
		geom.add(new Sphere(new Vector3(0, 1, 0), 0.75f));
		geom.add(new Sphere(new Vector3(1, 2, 1), 0.25f));
		

		geom.add(new Sphere(new Vector3(0, 1, 0), 1));
		geom.add(new Plane(Vector3.zero, Vector3.up));
		geom.add(new Plane(new Vector3(-2, 0, 0), new Vector3(1, 0, 0)));
		geom.add(new Plane(new Vector3(2, 0, 0), new Vector3(-1, 0, 0)));
		geom.add(new Plane(new Vector3(0, 0, -2), new Vector3(0, 0, 1)));
		
		ArrayList<Light> lights = new ArrayList<Light>();
		lights.add(new PointLight(new Vector3(0, 4, 0), new Vector3(100)));
		lights.add(new AmbientLight(new Vector3(1)));
		
		ArrayList<Camera> cameras = new ArrayList<Camera>();
		cameras.add(new Camera(new Vector3(0, 5, 10), new Vector3(0, 1, 0), 60));
		
		return new Scene(geom, lights, cameras);
	}
}
