package raytracer;

import java.awt.image.BufferedImage;
import java.util.Arrays;


public class FrameBuffer {

	public int width;
	public int height;
	Vector3[][] pixels;

	private float exposure;

	public FrameBuffer(int width, int height) {
		this.width = width;
		this.height = height;
		pixels = new Vector3[height][width];
		for(int h = 0; h < height; h++) {
			Arrays.fill(pixels[h], Vector3.zero);
		}
	}

	public BufferedImage getImage(float exposure) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		this.exposure = exposure;
		
		for(int h = 0; h < height; h++ ){ 
			for(int w = 0; w < width; w++) {
				int adjustedHeight = height - 1 - h; //BufferedImage's (0,0) is TopLeft, ours is BottomLeft
				image.setRGB(w, h, getRGB( pixels[adjustedHeight][w] ));
			}
		}

		return image;

	}

	public float getAutoExposure() {

		float highest = 0f;
		Vector3[] flattenPixels = getFlattenedPixels();
		for(Vector3 pixel : flattenPixels) {
			float h = (float)(Math.max(Math.max(pixel.x, pixel.y), pixel.z));
			if (h > highest) highest = h;
		}

		if(highest == 0) highest = 1f;
		System.out.println("Highest value in framebuffer is " + highest);

		float result = (float) -(Math.log(1f / 255) / highest);
		System.out.println("Autoexposure value is " + result);

		return result;
	}

	public int getRGB(Vector3 pixel) {

		int r = transfer(pixel.x);
		int g = transfer(pixel.y);
		int b = transfer(pixel.z);

		return r * 65536 + g * 256 + b;
	}

	public int transfer(float value) {
		return (int) Math.floor(255 * (1 - Math.exp(-this.exposure * value)));
	}

	public Vector3[] getFlattenedPixels() {
		Vector3[] result = new Vector3[width * height];

		int indexCount = 0;

		for(Vector3[] row : pixels) {
			for(Vector3 pixel : row) {
				result[indexCount] = pixel;
				indexCount++;
			}
		}

		return result;
	}

}
