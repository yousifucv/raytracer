package raytracer;

public class Ray {
	
	public Vector3 origin;
	public Vector3 direction;
	
	public Ray(Vector3 origin, Vector3 direction) {
		super();
		this.origin = origin;
		this.direction = direction.norm();
	}
	
	@Override
	public String toString() {
		return "Ray [origin=" + origin + ", direction=" + direction + "]";
	}
	
	
	
} 