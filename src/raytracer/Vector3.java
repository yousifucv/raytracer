package raytracer;

public class Vector3 {

	public static final Vector3 zero = new Vector3(0);
	public static final Vector3 one = new Vector3(1);
	public static final Vector3 up = new Vector3(0,1,0);
	public static final Vector3 down = new Vector3(0,-1,0);
	
	public float x;
	public float y;
	public float z;

	public Vector3(float x, float y, float z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3(float v) {
		super();
		this.x = this.y = this.z = v;
	}

	@Override
	public String toString() {
		return "Vector3 [x=" + x + ", y=" + y + ", z=" + z + "]";
	}

	public Vector3 add(Vector3 other) {
		return new Vector3(x + other.x, y + other.y, z + other.z);
	}
	
	public Vector3 sub(Vector3 other) {
		return new Vector3(x - other.x, y - other.y, z - other.z);
	}

	public Vector3 neg() {
		return new Vector3(-x,-y,-z);
	}

	public float dot(Vector3 other) {
		return x*other.x + y*other.y + z*other.z;
	}
	
	public Vector3 cross(Vector3 other) {
		return new Vector3(
				y*other.z-z*other.y,
				z*other.x-x*other.z,
				x*other.y-y*other.x);
	}

	public Vector3 multi(float scale) {
		return new Vector3(x*scale, y*scale, z*scale);
	}

	public float magSq() {
		return (x*x + y*y + z*z);
	}

	public float mag() {
		return (float)Math.sqrt(x*x + y*y + z*z);
	}

	public Vector3 norm() {
		float mag = mag();
		return new Vector3(x/mag, y/mag, z/mag);
	}

	public void normalize() {
		float mag = mag();
		x /= mag;
		y /= mag;
		z /= mag;
	}


}
