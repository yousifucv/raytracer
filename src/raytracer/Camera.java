package raytracer;

import geometry.Geometry;
import geometry.Intersection;

import java.util.ArrayList;

import lights.Light;
import scene.Scene;

public class Camera {

	public Vector3 position;
	public Vector3 target;
	public float h_fov;
	public float aspect;

	public Vector3 viewVector;
	public Vector3 plane_xVector;
	public Vector3 plane_yVector;

	private float plane_xVectorScale;

	public Camera(Vector3 position, Vector3 target, float h_fov) {
		super();
		this.position = position;
		this.target = target;
		this.h_fov = h_fov;

		this.viewVector = target.sub(position);

		this.plane_xVectorScale = (float)Math.tan(h_fov/2)*viewVector.mag();

		this.plane_xVector = viewVector.cross(Vector3.up).norm().multi(plane_xVectorScale);
	}

	public void render(Scene scene, FrameBuffer frameBuffer) {
		float aspect = 1f * frameBuffer.width / frameBuffer.height;

		Vector3 upVector = this.plane_xVector.cross(viewVector).norm().multi(plane_xVectorScale / aspect);
		
		float MAX_X = frameBuffer.width - 1;
		float MAX_Y = frameBuffer.height - 1;
		
		for(int y = 0; y <= MAX_Y; y++) {
			for(int x = 0; x <= MAX_X; x++) {
				Ray ray = getRay(x * 2f / MAX_X - 1, y * 2f / MAX_Y - 1, upVector);

				ArrayList<Intersection> intersections = new ArrayList<Intersection>();
				
				for(Geometry g : scene.geometry) {
					Intersection i = g.intersectedBy(ray);
					if(i != null) intersections.add(i);
				}
				if(!intersections.isEmpty()) {
					intersections.sort(new Intersection.DistanceComparator());
					Intersection nearest = intersections.get(0);
					
					Vector3 accumulator = Vector3.zero;
					for(Light light : scene.lights) {
						Vector3 lightContribution = light.getIlluminance(nearest, scene.geometry);
						accumulator = accumulator.add(lightContribution);
					}
					frameBuffer.pixels[y][x] = accumulator;
				}
			}
		}
		
	}

	public Ray getRay(float viewX, float viewY, Vector3 upVector) {

		Vector3 xDisplacement = plane_xVector.multi(viewX);
		Vector3 yDisplacement = upVector.multi(viewY);
		
		return new Ray(position, viewVector.add(xDisplacement).add(yDisplacement));
	}
	
	@Override
	public String toString() {
		return "Camera [position=" + position + ", target=" + target
				+ ", h_fov=" + h_fov + "]";
	}






}
