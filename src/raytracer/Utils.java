package raytracer;

public final class Utils {

	public static final float TAU = (float)(Math.PI * 2); 
	
	public static float map(float value, float srcStart, float srcEnd, float dstStart, float dstEnd) {
		float srcRange = srcEnd - srcStart;
		float dstRange = dstEnd - dstStart;
		float ratio = (value - srcStart) / srcRange;
		return dstStart + ratio * dstRange;
	}
	
}
