package raytracer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import scene.Scene;

public class Main {

	static FrameBuffer frameBuffer = new FrameBuffer(1000,1000);

	public static void render() {
		Scene scene = Scene.spiralScene();
		Camera camera = scene.cameras.get(0);

		long startNanoTime = System.nanoTime();

		System.out.println("Rendering started...");

		camera.render(scene, frameBuffer);

		long endNanoTime = System.nanoTime();

		System.out.println("Completed in " + ((endNanoTime - startNanoTime)/1_000_000) + " ms.");
		System.out.println("-----");
	}


	public static void main(String[] args) {
		
		render();

		JFrame frame = new JFrame() {

			
			{ //initialize
				
			BufferedImage img = frameBuffer.getImage(frameBuffer.getAutoExposure());
			float scaleFactor = (float)(Math.min(1080f / frameBuffer.width,  500f / frameBuffer.height));
			Dimension displaySize = new Dimension((int)(frameBuffer.width * scaleFactor), (int)(frameBuffer.height * scaleFactor));
			

			Component component = new Component() {
				{
					setPreferredSize(displaySize);
					setResizable(true);
					setIgnoreRepaint(true);
				}
				@Override
				public void paint(Graphics g) {
					g.drawImage(img, 0, 0, displaySize.width, displaySize.height, null);
				}
				
			};
				System.out.println(displaySize.width + " " + displaySize.height);
				add(component);
				addKeyListener(new KeyListener() {
					@Override
					public void keyTyped(KeyEvent e) {
					}
					@Override
					public void keyReleased(KeyEvent e) {
						if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
							dispose();
						}
						else if(e.getKeyCode() == KeyEvent.VK_ENTER) {
							saveNextImage(img);
							setTitle(getTitle() + " [SAVED]");
						}
					}
					@Override
					public void keyPressed(KeyEvent e) {
					}
				});
			}

		};
		
		frame.setTitle("Fojar's Raytracer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

		
	}
	
	public static void saveNextImage(BufferedImage image) {
		FileFilter filter = new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isFile() && f.getName().startsWith("image");
			}
		};
		File[] fileNames = new File("images/").listFiles(filter);
		int highest = 0;
		for(File f : fileNames) {
			int num = Integer.parseInt(f.getName().substring(5, 9));
			if(num > highest) highest = num;
		}
		
		int next = highest + 1;
		
		try {
			File outputfile = new File(String.format("images/image%04d.png", next));
			ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
