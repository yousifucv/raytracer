package geometry;

import raytracer.Ray;
import raytracer.Vector3;

public class Sphere extends Geometry {

	public Vector3 centre;
	public float radius;

	public Sphere(Vector3 centre, float radius) {
		super();
		this.centre = centre;
		this.radius = radius;
	}

	public boolean intersects(Ray ray) {

		Vector3 o = ray.origin.sub(centre);
		Vector3 d = ray.direction;

		float A = 1f;
		float B = o.dot(d) * 2;
		float C = o.magSq() - radius * radius;

		float discrim = B*B - 4 * A * C;

		return discrim >= 0;
	}

	@Override
	public String toString() {
		return "Sphere [centre=" + centre + ", radius=" + radius + "]";
	}

	@Override
	public Intersection intersectedBy(Ray ray) {
		
		Vector3 o = ray.origin.sub(centre);
		Vector3 d = ray.direction;

		float A = 1f;
		float B = o.dot(d) * 2;
		float C = o.magSq() - radius * radius;

		float discrim = B*B - 4 * A * C;

		if(discrim >= 0) {
			float sqrt = (float)Math.sqrt(discrim);
			float t0 = (-B + sqrt) / (2 * A);
			float t1 = (-B - sqrt) / (2 * A);

			float t = (float)Math.min(t0, t1);

			if(t > 0) return new SphereIntersection(ray, t, this);
		}
		
		return null;
	}
}

class SphereIntersection extends Intersection {

	private Sphere sphere;

	public SphereIntersection(Ray ray, float distance, Sphere sphere) {
		super(ray, distance, sphere);
		this.sphere = sphere;
	}

	@Override
	public Vector3 normal() {
		if(__normal == null) {
			__normal = location().sub(sphere.centre).norm();
		}
		return __normal;
	}

}