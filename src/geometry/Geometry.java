package geometry;

import raytracer.Ray;

public abstract class Geometry {
	
	abstract public Intersection intersectedBy(Ray ray);
}
