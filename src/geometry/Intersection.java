package geometry;

import java.util.Comparator;

import raytracer.Ray;
import raytracer.Vector3;

public abstract class Intersection {
	
	public Ray ray;
	public float distance;
	public Geometry geometry;
	
	//lazy
	protected Vector3 __normal;
	protected Vector3 __location;
	
	public Intersection(Ray ray, float distance, Geometry geometry) {
		super();
		this.ray = ray;
		this.distance = distance;
		this.geometry = geometry;
	}

	public Vector3 location() {
		if(__location==null) {
			__location = ray.origin.add(ray.direction.multi(distance));
		}
		return __location;
	}
	
	public Vector3 normal() {
		return __normal;
	}
	
	public Vector3 setNormal(Vector3 normal) {
		return __normal = normal;
	}
	
	
	public static class DistanceComparator implements Comparator<Intersection> {

		@Override
		public int compare(Intersection o1, Intersection o2) {
			if( (o1.distance - o2.distance) < 0) return -1;
			else if( (o1.distance - o2.distance) > 0) return 1;
			else return 0;
		}
		
	}

}
