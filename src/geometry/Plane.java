package geometry;

import raytracer.Ray;
import raytracer.Vector3;

public class Plane extends Geometry {

	public final Vector3 origin;
	public final Vector3 normal;

	public Plane(Vector3 origin, Vector3 normal) {
		super();
		this.origin = origin;
		this.normal = normal;
	}



	@Override
	public Intersection intersectedBy(Ray ray) {
		
		float denom = ray.direction.dot(normal);
		if(denom >= 0) 
			return null;
		
		float numer = (origin.sub(ray.origin)).dot(normal);
		float t = numer / denom;
		
		return new PlaneIntersection(ray, t, this);
	}

}

class PlaneIntersection extends Intersection {
	
	public PlaneIntersection(Ray ray, float distance, Plane plane) {
		super(ray, distance, (Geometry)plane);
		this.setNormal(plane.normal);
	}

}